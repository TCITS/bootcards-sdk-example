﻿@echo off

SET src=###%1%###
SET src=%src:"###=%
SET src=%src:###"=%
SET src=%src:###=%

SET dest=###%2%###
SET dest=%dest:"###=%
SET dest=%dest:###"=%
SET dest=%dest:###=%

mkdir "%dest%\..\www" >nul 2>nul
"%src%\_Build\robocopy.exe" "%dest%\Content" "%dest%\..\www" /MIR /R:0 /NP /NJH /NJS /NDL /XD core 
"%src%\_Build\robocopy.exe" "%dest%\.." "C:\tc-projects\1.7.4\Build\Debug\applications\stockit" /MIR /R:0 /NP /NJH /NJS /NDL /XO 
ver > nul