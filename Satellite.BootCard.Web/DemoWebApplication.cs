﻿using Satellite.Demo.Web.Helpers;
using Universal.Access;
using Universal.Common.Applications;
using Universal.Common.Extensions;
using Universal.Common.Menu;

namespace Satellite.Demo.Web
{
    [ApplicationExtension(ApplicationGuidString, ApplicationExtensionGuid, "Demo Web Application", "Demo Web Application")]
    public class DemoWebApplication : WebApplication
    {
        public const string ApplicationExtensionGuid = "D16F49FA-DE5C-4B10-95D3-B11BECFC5269";

        public const string ApplicationGuidString = "2C89997E-0CC1-4E27-86C9-26D12C81844C";
        public static readonly GuidString ExtensionGuid = new GuidString(ApplicationExtensionGuid);
        public static readonly GuidString ApplicationGuid = new GuidString(ApplicationGuidString);
        public override IMenu GetMenu()
        {
            return SiteHelper.DemoMenu;
        }

        public override void SetMenu(IMenu menu)
        {
            SiteHelper.DemoMenu = menu;
        }
    }
}
