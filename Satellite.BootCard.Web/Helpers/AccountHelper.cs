﻿using System.Text;
using Universal.Access.Mvc;
using Universal.Access.WebServer;

namespace Satellite.Demo.Web.Helpers
{
    public static class AccountHelper
    {
        public static string GenerateLoginForm(ISessionContext context, IViewData data, bool requireOrganisation = true,
                                               bool requireUser = true, string redirect = "/")
        {
            var builder = new StringBuilder();

            builder.Append("<form id='login-form' name='login-form' enctype='multipart/form-data'>");

            builder.Append("<input type='hidden' name='redirect' value='" + redirect + "'> ");

            builder.Append("<div class='login-message' id='login-message'>");
            builder.Append(context.ShowMessages());
            builder.Append("</div>");

            if (requireOrganisation && requireUser)
            {
                //builder.Append(FormControls.TextInput("Organisation", (string)data["Organisation"], "Organisation", "Organisation", true, tooltip: "Please select Organisation", icon: "fa fa-users"));
            }

            if (requireUser)
            {
                builder.Append("<label class='h6'>Username</label>");
                builder.Append("<input type='text' id='UserName' name='UserName' data-trigger='change' class='form-control' placeholder='User Name' required>");
            }

            builder.Append("<label class='h6'>Password</label>");
            builder.Append("<input type='password' id='Password' name='Password' data-trigger='change' class='form-control' placeholder='Password' required>");

            builder.Append("<p class='help-block'><a href='" + context.ApplicationRoute + "/forgotpassword'>Forgot password?</a></a>");

            builder.Append("<div class='form-buttons'>");
            builder.Append("<span class='pull-left'><label class='checkbox' for='Remember'><input type='checkbox' id='Remember' name='Remember' data-toggle='checkbox' checked />Stay signed in</label></span>");
            builder.Append("<span class='pull-right'><button id='LoginButton' class='btn btn-primary' type='submit'>Sign in</button></span>");
            builder.Append("</div>");
            builder.Append("</form>");

            return (builder.ToString());
        }

        public static string GenerateForgotPasswordForm(ISessionContext context, IViewData data, bool requireOrganisation = true,
            string redirect = "/")
        {
            var builder = new StringBuilder();
            builder.Append(
                "<form id='ForgotPasswordForm' name='ForgotPasswordForm' enctype='multipart/form-data'>");

            builder.Append("<input type='hidden' name='redirect' value='" + redirect + "'> ");

            builder.Append("<h3>Forgot Password</h3>");

            if (requireOrganisation)
            {
                builder.Append("<label class='h6'>Organisation</label>");
                builder.Append("<input type='text' id='Organisation' name='Organisation' value='" + (string)data["Organisation"] + "' class='form-control' required>");
            }

            builder.Append("<label class='h6'>Email</label>");
            builder.Append("<input type='text' id='Email' name='Email' value='" + (string)data["Email"] + "' class='form-control' required>");

            builder.Append("<span class='timeline-seperator text-center center-block text-primary'> <span class='font-sm'>OR</span></span><br/>");

            builder.Append("<label class='h6'>User Name</label>");
            builder.Append("<input type='text' id='UserName' name='UserName' value='" + (string)data["UserName"] + "' class='form-control' required>");

            builder.Append("<div class='form-buttons'>");
            builder.Append("<span class='pull-left' style='padding-top:9px;'><a href='" + context.ApplicationRoute + "/login'>I remembered my password!</a></span>");
            builder.Append("<span class='pull-right'><button id='ResetPassword' class='btn btn-primary' type='submit'>Reset Password</button></span>");
            builder.Append("</div>");

            builder.Append("</form>");

            return (builder.ToString());
        }

        public static string GenerateActivationCodeForm(IViewData data, string redirect = "/")
        {
            var builder = new StringBuilder();
            builder.Append(
                "<form id='ActivationCodeForm' name='ActivationCodeForm' enctype='multipart/form-data'>");
            builder.Append("<h3>Enter activation code</h3>");

            builder.Append("<input type='hidden' name=' redirect' value='" + redirect + "'> ");

            builder.Append("<label class='h6'>Activation Code</label>");
            builder.Append("<input type='password' id='ActivationCode' name='ActivationCode' value='" + (string)data["ActivationCode"] + "' class='form-control' required>");

            builder.Append("<div class='form-buttons'>");
            builder.Append(
                "<span class='pull-right'><button id='CheckCode' class='btn btn-primary' type='submit'> Reset Password </button></span>");
            builder.Append("</div>");
            builder.Append("</form>");

            return (builder.ToString());
        }

        public static string GenerateResetPasswordForm(string code, string redirect = "/")
        {
            var builder = new StringBuilder();

            builder.Append("<form id='NewPasswordForm' name='NewPasswordForm' enctype='multipart/form-data' >");
            builder.Append("<h3>Enter a new password</h3>");

            builder.Append("<input type='hidden' name='redirect' value='" + redirect + "'> ");
            builder.Append("<input type='hidden' name='ActivationCode' value='" + code + "'> ");

            builder.Append("<label class='h6'>Password</label>");
            builder.Append("<input type='password' id='Password' name='Password' data-trigger='change' class='form-control' placeholder='Password' required>");

            builder.Append("<div class='form-buttons'>");
            builder.Append("<span class='pull-right'><button id='SetPassword' class='btn btn-primary' type='submit'> Set Password </button></span>");
            builder.Append("</div>");
            builder.Append("</form>");

            return builder.ToString();
        }
    }
}
