﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Universal.Access;
using Universal.Access.Data;
using Universal.Access.Definition;
using Universal.Access.Entities;
using Universal.Access.WebServer;
using Universal.Common;
using Universal.Common.Density;
using Universal.Common.Entities;
using Universal.Common.License;
using Universal.Common.Menu;
using Universal.Common.Parameters;
using Universal.Common.Platform;
using Universal.Common.Utils;
using Universal.Common.WebServer;
using Universal.Common.WebServer.Views;

namespace Satellite.Demo.Web.Helpers
{
    public class ThemeHelper
    {

        public static IObjectParameterHelperContext GetContext(IAccessInstance entity, ISessionContext sessionContext)
        {
            return GetContext((IEntity) entity, sessionContext);
        }

        public static IObjectParameterHelperContext GetContext(IEntity entity, ISessionContext sessionContext)
        {
            ParameterContext context = ParameterContext.For(entity);
            if (context != null)
            {
                context.SessionParameters = sessionContext.GetParameters();
            }
            return (context);
        }

        

        public static string GenerateExtensionStep(IEnumerable<IExtension> extensions, int stepNumber = 1,
                                                   string headerText = "Choose Extension")
        {
            var builder = new StringBuilder();
            builder.Append("<input type=\"hidden\" name=\"ExtensionGuid\" id=\"ExtensionGuid\" value=\"\" />");
            builder.Append("<br/>");
            builder.Append("<br/>");
            builder.Append("<h4 class=\"wizard-step-header\">");
            builder.Append("<strong>Step " + stepNumber + "</strong> - " + headerText);
            builder.Append("</h4>");
            builder.Append("<br/>");
            builder.Append("<div class=\"col-sm-12\">");
            builder.Append("<div class=\"wizard-extension-table\">");
            builder.Append(
                "<table class=\"table table-striped table-bordered table-hover \" id=\"extension-table\" width=\"100%\">");
            builder.Append("<thead>");
            builder.Append("<tr>");
            builder.Append("<th class=\"hidden-column\">ExtensionGuid</th>");
            builder.Append("<th>Extension Name</th>");
            builder.Append("</tr>");
            builder.Append("</thead>");
            builder.Append("<tbody>");
            foreach (IExtension extension in extensions)
            {
                builder.Append("<tr>");
                builder.Append("<td class=\"hidden-column\">" + extension.ExtensionGuid + "</td>");
                builder.Append("<td>" + extension.DisplayName + "</td>");
                builder.Append("</tr>");
            }
            builder.Append("</tbody>");
            builder.Append("</table>");
            builder.Append("</div>");
            builder.Append("</div>");

            return builder.ToString();
        }

        /// <summary>
        ///     Generates the footer contents.
        /// </summary>
        /// <returns>
        ///     The <see cref="string" />.
        /// </returns>
        /// <remarks>
        /// </remarks>
        public static string GenerateFooterContents(ISessionContext sessionContext)
        {
            var builder = new StringBuilder();

            builder.Append("<div class='row' style='margin: 0px; padding: 0px;' >");
            builder.Append("<div class='col-xs-10 col-sm-10 font-sm hidden-mobile'>");
            builder.Append("<span class='footer-text'><span class='hidden-mobile'>Copyright © 2004 - 2015   PTY LTD. All rights reserved.</span>");
            builder.Append("</br><span class='hidden-mobile' ><p class='small text-muted'>Built with <span style='color:red'>&#9829;</span> by <a href='http://www.universalplatform.com'>Universal Platform</a></p></span>");
            builder.Append("</div>");
            builder.Append("</div>");

            builder.Append("</div>");

            if (InternalService.PlatformContext.PlatformLicense.CheckForLicenseWarning() ||
                InternalService.PlatformContext.PlatformLicense.CheckForLicenseError())
            {
                builder.Append("<div id=\"notification-bar\">");
                // show for license expiry or type otherwise display:none
            }
            else
            {
                builder.Append("<div id=\"notification-bar\" style=\"display:none;\">");
            }

            builder.Append("<br/>");
            builder.Append("<br/>");
            builder.Append("<br/>");
            builder.Append("<div class=\"navbar navbar-fixed-bottom navbar-inverse\">");

            if (InternalService.PlatformContext.PlatformLicense.CheckForLicenseError())
            {
                builder.Append("<div class=\"errorbar-inner\">");

                if (InternalService.PlatformContext.PlatformLicense.UserHardLimit)
                {
                    builder.Append(
                        string.Format(
                            "You have exceeded the number of active users permitted by your license. You currently have {0} active users out of {1} allowed. Please contact your supplier or you will be unable to continue using some features of the Universal Platform.",
                            InternalService.PlatformContext.PlatformLicense.CurrentActiveUsers,
                            InternalService.PlatformContext.PlatformLicense.UserLimit));
                    builder.Append("<br />");
                }

                if (InternalService.PlatformContext.PlatformLicense.OrganisationHardLimit)
                {
                    builder.Append(
                        string.Format(
                            "You have exceeded the number of organsiations permitted by your license. You currently have {0} organisations out of {1} allowed. Please contact your supplier or you will be unable to continue using some features of the Universal Platform.",
                            InternalService.PlatformContext.PlatformLicense.CurrentOrganisations,
                            InternalService.PlatformContext.PlatformLicense.OrganisationLimit));
                    builder.Append("<br />");
                }

                builder.Append("</div>");
            }

            if (!InternalService.PlatformContext.PlatformLicense.CheckForLicenseWarning())
            {
                builder.Append("<div id=\"warning-bar\" class=\"warningbar-inner\" style=\"display:none\">");
            }
            else
            {
                builder.Append("<div id=\"warning-bar\" class=\"warningbar-inner\">");
            }

            if (InternalService.PlatformContext.PlatformLicense.UserSoftLimit)
            {
                builder.Append(
                    string.Format(
                        "You have exceeded the number of active users permitted by your license. You currently have {0} active users out of {1} allowed. Please contact your supplier or you will be unable to continue using some features of the Universal Platform.",
                        InternalService.PlatformContext.PlatformLicense.CurrentActiveUsers,
                        InternalService.PlatformContext.PlatformLicense.UserLimit));
                builder.Append("<br />");
            }

            if (InternalService.PlatformContext.PlatformLicense.LicenseType != LicenseType.Production)
            {
                builder.Append(
                    string.Format(
                        "The system is operating under a {0} license, and cannot be used for production purposes.",
                        InternalService.PlatformContext.PlatformLicense.LicenseType));
                builder.Append("<br />");
            }

            if (InternalService.PlatformContext.PlatformLicense.ExpiryDate != null)
            {
                double daysUntilExpiry =
                    (InternalService.PlatformContext.PlatformLicense.ExpiryDate.Value.Date - DateTime.Now.Date)
                        .TotalDays;
                if (InternalService.PlatformContext.PlatformLicense.ExpiryDate.Value.Date != new DateTime().Date)
                {
                    if (daysUntilExpiry <= 30 && daysUntilExpiry > 0)
                    {
                        builder.Append(string.Format("Your Universal Platform License will expire in {0} day{1}.",
                                                     daysUntilExpiry, daysUntilExpiry == 1 ? string.Empty : "s"));
                    }
                    else if (daysUntilExpiry <= 0 && daysUntilExpiry > -7)
                    {
                        builder.Append(
                            string.Format(
                                "Your Universal Platform License has expired. Please contact your supplier within {0} day{1} or you will be unable to continue using the Universal Platform.",
                                daysUntilExpiry + 7,
                                daysUntilExpiry == -6 ? string.Empty : "s"));
                    }
                    else if (daysUntilExpiry <= -7)
                    {
                        builder.Append(
                            "Your Universal Platform License has expired. Please contact your supplier or you will be unable to continue using the Universal Platform.");
                    }
                }
            }

            builder.Append("</div>"); // warning-bar
            builder.Append("</div>"); // navbar
            builder.Append("</div>"); // notification-bar
            builder.Append("</div>"); // footer

            return builder.ToString();
        }

        public static string GenerateChatMenu(ISessionContext sessionContext)
        {
            var builder = new StringBuilder();

            builder.Append("<li id='platform-chat-menu' class='chat-users top-menu-invisible open'>");
            builder.Append(
                "<a id='platform-chat-menu-link' href='#'><i class='fa fa-lg fa-fw fa-comment-o'></i> <span class='menu-item-parent'>Chat</span></a>");
            builder.Append("<ul style='display: block;'>");

            //<em class='bg-color-pink flash animated'>!</em>

            builder.Append("<li class='active'>");
            builder.Append("<div id='platform-chat-users'  style='padding-bottom: 5px;' class='display-users'>");

            //builder.Append("<input id='platform-chat-user-filter' type='text' placeholder='Filter' class='form-control chat-user-filter'></input>");
            //builder.Append("<a data-content='Yoh' data-html='true' data-placement='right' data-rel='popover-hover' data-chat-alertshow='true' data-chat-alertmsg='Sadi Orlaf is in a meeting. Please do not disturb!' data-chat-status='busy' data-chat-lname='Orlaf' data-chat-fname='Sadi' data-chat-id='cha1' class='usr' href='#' data-original-title='' title=''><i></i>Sadi Orlaf</a>");
            //builder.Append("<a data-content='Yoh' data-html='true' data-placement='right' data-rel='popover-hover' data-chat-alertshow='true' data-chat-alertmsg='Sadi Orlaf is in a meeting. Please do not disturb!' data-chat-status='busy' data-chat-lname='Orlaf' data-chat-fname='Sadi' data-chat-id='cha1' class='usr' href='#' data-original-title='' title=''><i></i>Sadi Orlaf</a>");

            builder.Append("</div>");

            builder.Append("</li>");
            builder.Append("</ul>");
            builder.Append("</li>");

            return builder.ToString();
        }

        public static string GenerateMenu(ISessionContext sessionContext, IMenu menu)
        {
            var builder = new StringBuilder();

            List<IDensityMenuItem> parentMenuItems = menu.GetMenu();
            IDensityMenuItem currentMenuItem = menu.GetCurrentMenuItem();

            if (parentMenuItems != null)
            {
                foreach (IDensityMenuItem parentMenuItem in parentMenuItems)
                {
                    GenerateNavItem(sessionContext, menu, builder, parentMenuItem, currentMenuItem);
                }
            }

            return builder.ToString();
        }

        /// <summary>
        ///     Generates the nav item.
        /// </summary>
        /// <param name="builder">
        ///     The builder.
        /// </param>
        /// <param name="menuItem">
        ///     The menu item.
        /// </param>
        /// <param name="currentMenuItem">
        ///     The current menu item.
        /// </param>
        /// <remarks>
        /// </remarks>
        public static void GenerateNavItem(ISessionContext sessionContext, IMenu menu, StringBuilder builder,
                                           IDensityMenuItem menuItem, IDensityMenuItem currentMenuItem)
        {
            if (menuItem.ShowInList)
            {
                string modulePath = string.Empty;
                if (menuItem.Module != null)
                {
                    modulePath = menuItem.Module.FullPath;
                }

                if (!string.IsNullOrEmpty(modulePath))
                {
                    if (menuItem.MenuKey.Equals(currentMenuItem.MenuKey))
                    {
                        menu.SetCurrentMenu(menuItem);
                        menuItem.IsActive = true;
                    }
                    else
                    {
                        menuItem.IsActive = false;
                    }
                }

                string enabledClass = string.Empty;
                IDensityModule module = menuItem.Module;
                if (module != null)
                {
                    if (!string.IsNullOrEmpty(module.ModuleClass))
                    {
                        //if (!GetLicenseHelperSession(sessionContext).IsFeatureAvailableByModuleClass(module.ModuleClass))
                        //{
                        //    enabledClass = "disabled";
                        //    modulePath = "#";
                        //}
                    }
                }

                builder.Append("<li");
                if (menuItem.IsActive)
                {
                    builder.Append(" class=\"active\"");
                }

                builder.Append(">");

                builder.Append("<a class='mobile-nav'");
                if (!string.IsNullOrEmpty(modulePath))
                {
                    builder.Append("href=\"" + modulePath + "\"");
                }

                builder.Append(" title=\"" + menuItem.Title + "\">");
                if (menuItem.Title.ToLowerInvariant() == "bar")
                {
                    builder.Append("<i class='fa fa-lg fa-fw fa-bar-chart'></i>");
                }
                if (menuItem.Title.ToLowerInvariant() != "bar")
                {
                    builder.Append("<i class='fa fa-lg fa-fw fa-home'></i>");
                }
                
                builder.Append("<span class=\"menu-item-parent " + enabledClass + "\" style='padding-top: 0px; margin-top: 3px;' >" + menuItem.Title + "</span></a>");
            }

            if (menuItem.MenuItems.Count > 0 && menuItem.ShowInList)
            {
                builder.Append("<ul>");
            }

            foreach (IDensityMenuItem subMenuItem in menuItem.MenuItems)
            {
                GenerateNavItem(sessionContext, menu, builder, subMenuItem, currentMenuItem);
            }

            if (menuItem.MenuItems.Count > 0 && menuItem.ShowInList)
            {
                builder.Append("</ul>");
            }

            if (menuItem.ShowInList) builder.Append("</li>");
        }

        public static string GeneratePermissionsContainer(string entityName, GuidString entityGuid,
                                                          IViewHelper viewhelper)
        {
            var builder = new StringBuilder();
            builder.Append("<fieldset>");
            builder.Append("<div id=\"permissions-container\">");
            builder.Append("<div id=\"permissions-placeholder\" >");
            builder.Append("</div>");
            builder.Append("</div>");
            builder.Append("</fieldset>");

            viewhelper.DocumentReady.Add(
                "$('a[data-toggle=\"tab\"]').on('shown.bs.tab', function (e) { if (e.target.id == \"security-tab\") { loadPermissions(\"" +
                entityName + "\",\"" + entityGuid + "\"); }});");

            return builder.ToString();
        }

        public static string GenerateSecurityFields(IAccessInstance entity)
        {
            return GenerateSecurityFields((IEntity) entity);
        }

        public static string GenerateWebApplicationDropDown()
        {
            var builder = new StringBuilder();

            builder.Append("<ul class=\"dropdown-menu\">");
            var webServer =
                (IWebServerPlatformService)
                InternalService.PlatformContext.Locator.GetInstance<IPlatformService>("WebServerPlatformService");

            foreach (WebApplicationDetail webApp in webServer.WebApplications)
                //foreach (IWebApplicationExtension webApp in webServer.WebApplications)
            {
                if (string.IsNullOrEmpty(webApp.Entity.DefaultRoute) || !webApp.Entity.EnabledFlag)
                {
                    continue;
                }

                builder.Append("<li>");
                builder.Append("<a href=\"" + webApp.Entity.DefaultRoute + "\">" +
                               webApp.Entity.Name + "</a>");
                builder.Append("</li>");
            }
            builder.Append("</ul>");

            return builder.ToString();
        }

        public static string GenerateWizardSteps(params string[] stepNames)
        {
            var builder = new StringBuilder();

            builder.Append("<div class=\"form-bootstrapWizard\">");
            builder.Append("<ul class=\"bootstrapWizard form-wizard\">");

            int stepNumber = 1;
            bool first = true;

            decimal totalSteps = stepNames.Length;
            decimal widthPerStep = Math.Round(100/totalSteps);

            foreach (string step in stepNames)
            {
                builder.Append("<li style=\"width:" + widthPerStep + "%\" ");

                if (first)
                {
                    builder.Append(" class=\"active\" ");
                }

                builder.Append(" data-target=\"#step" + stepNumber + "\">");
                builder.Append("<a href=\"#tab" + stepNumber + "\" data-toggle=\"tab\">");
                builder.Append("<span class=\"step\">" + stepNumber + "</span>");
                builder.Append("<span class=\"title\">" + step + "</span>");
                builder.Append("</a>");
                builder.Append("</li>");

                first = false;
                stepNumber++;
            }

            builder.Append("</ul>");
            builder.Append("<div class=\"clearfix\"></div>");
            builder.Append("</div>");

            return builder.ToString();
        }

        public static ILicenseHelper GetLicenseHelperSession(ISessionContext sessionContext)
        {
            string licenseHelperKey = "LICENSE_HELPER";

            IDictionary<string, object> userParmeters = sessionContext.GetUserParameters();

            ILicenseHelper helper = null;
            object helperObject = null;
            if (userParmeters.TryGetValue(licenseHelperKey, out helperObject))
            {
                helper = (ILicenseHelper) helperObject;
            }
            else
            {
                helper = InternalService.PlatformContext.PlatformLicense.CreateLicenseHelper();
                if (userParmeters.ContainsKey(licenseHelperKey))
                {
                    userParmeters[licenseHelperKey] = helper;
                }
                else
                {
                    userParmeters.Add(licenseHelperKey, helper);
                }
            }

            return (helper);
        }

        public static string PageTitle(string title)
        {
            if (string.IsNullOrEmpty(title))
            {
                return title;
            }

            string platformTitle = InternalService.PlatformContext.FullPlatformTitle;
            return string.Format("{0} - {1}", platformTitle, title);

            //if (string.IsNullOrEmpty(platformTitle))
            //{
            //   platformTitle = "Universal Platform [${UP-PlatformVersion}.${UP-PlatformBuild}]";
            //}

            //return string.Format("{0} - {1}", platformTitle.ExpandVariables(), title);
        }

        public static string GenerateNotifications(ISessionContext context)
        {
            if (context == null)
            {
                return string.Empty;
            }

            var builder = new StringBuilder();
            return builder.ToString();
        }


        public static string GenerateSearchBar()
        {
            var builder = new StringBuilder();
            builder.Append("<div class='search-bar'>");

            builder.Append("<form id='global-search-form' class='sidebar-search'>");
            builder.Append(
                "<input id='global-search-text' class='search-query dropdown-toggle' type='text' name='param' placeholder='Search ...' />");
            builder.Append("<button type='submit'><i class='fa fa-search'></i></button>");
            builder.Append(
                "<a href='javascript:void(0);' id='cancel-search-js' title='Cancel Search'><i class='fa fa-times'></i></a>");
            builder.Append("<div id='global-search-result'></div>");
            builder.Append("</form>");

            builder.Append("</div>");

            return builder.ToString();
        }

        public static string SideBarFooter(ISessionContext context)
        {
            var builder = new StringBuilder();
            builder.Append("<div class='air air-bottom sidebar-footercontainer hidden-mobile'>");

            builder.Append("<div class='sidebar-footer'>");
            builder.Append("<div id='logout' class='btn-header btn-sidebar transparent pull-right'>");
            builder.Append(
                string.Format(
                    "<span><a href='{0}/logout' title='Sign Out' data-action='userLogout' data-logout-msg='You can improve your security further after logging out by closing this opened browser'><i class='fa fa-sign-out'></i></a></span>",
                    context.ApplicationRoute));
            builder.Append("</div>");

            builder.Append("<div id='fullscreen' class='btn-header btn-sidebar transparent pull-left hidden-mobile'>");
            builder.Append(
                "  <span><a href='javascript:void(0);' data-action='launchFullscreen' title='Full Screen'><i class='fa fa-arrows-alt'></i></a></span>");
            builder.Append("</div>");

            builder.Append("</div></div>");
            return builder.ToString();
        }

        public static string MainMenu(ISessionContext context, IMenu menu, string id = "UserMenu", string title = "Menu")
        {
            //${CallistoHelper.GenerateWebApplicationDropDown()}
            var builder = new StringBuilder();
            var content = new StringBuilder();
            var toolBar = new StringBuilder();
            var script = new StringBuilder();

            IDensityMenuItem parentMenuItem = menu.GetRootMenuItem();

            title = parentMenuItem.Title;

            // user info
            IWebSession webSession = menu.SessionContext.WebSession;

            //content.Append(GenerateSearchBar());

            content.Append("<nav>");
            content.Append("<ul class='nav navbar-nav'>");
            content.Append(GenerateMenu(menu.SessionContext, menu));

            //content.Append(GenerateChatMenu(menu.SessionContext));

            content.Append("</ul>");
            content.Append("</nav>");

            script.Append("$('.usermenu-filter').click(function (e) { e.stopPropagation(); }); ");
            script.Append(
                "$('#usermenu-filter-input').keyup(function (e) { var code = e.keyCode ? e.keyCode : e.which; if (code == 27) { $('#usermenu-filter-input').val(''); } filterUserMenu($('#usermenu-filter-input').val()); });");
            script.Append(
                "$('#usermenu-filter-input').addClear({ top: 0, right: 6, onClear: function () { $('#usermenu-filter-input').val(''); filterUserMenu(''); } }); ");

            script.Append("$('.panel-heading .pod-fa fa-menu-collapse').click(function() { ");
            script.Append("$(this).parents( '.panel:first' ).css( 'width', 38 ); ");
            script.Append("$(this).parents( '.panel:first' ).find( '.panel-body' ).toggle(); ");
            script.Append("}); ");

            builder.Append(content);

            return (builder.ToString());
        }

        public static string GenerateHelpLink(IMenu menu)
        {
            IDensityMenuItem currentParentMenuItem = menu.GetCurrentMenuItem();
            List<IDensityMenuItem> currentMenuItems = currentParentMenuItem.MenuItems.FindAll(x => x.IsActive);

            string helpLink = "";

            if ((currentParentMenuItem.MenuItems.Count == 0 && !string.IsNullOrEmpty(currentParentMenuItem.HelpLink)) ||
                currentMenuItems.Count > 1)
            {
                helpLink = currentParentMenuItem.HelpLink;
            }
            else if (currentMenuItems.Count == 1 && !string.IsNullOrEmpty(currentMenuItems[0].HelpLink))
            {
                helpLink = currentMenuItems[0].HelpLink;
            }

            var builder = new StringBuilder();
            builder.Append("<span class='help-container'><span><a target='blank' href='" + helpLink +
                           "' ><i class='fa fa-question-circle fa-2x'></i></a></span></span>");
            return builder.ToString();
        }

        public static string GenerateShortcutButton(ISessionContext context)
        {
            var builder = new StringBuilder();
            builder.Append("<div class='shortcut-container'>");
            builder.Append("<div class='btn-group btn-shortcut pull-left'>");

            builder.Append(
                "<a class='btn btn-default' data-action='toggleShortcut'><i class='glyphicon glyphicon-cog'></i> <i class='fa fa-caret-down'></i></a>");

            builder.Append("</div>");
            builder.Append("</div>");
            //content.Append(string.Format("<a href='javascript:void(0);' id='show-shortcut' data-action='toggleShortcut' rel='popover-hover' data-placement='bottom' data-original-title='{0}' data-content='{1}' data-html='true'>", webSession.DisplayName, userPopUp)); 
            /* builder.Append(string.Format("<a href='javascript:void(0);' id='show-shortcut' data-action='toggleShortcut'>", webSession.DisplayName, userPopUp));
           builder.Append("<img src='/up-console/public/img/avatars/male.png' alt='me' class='online' />");
           builder.Append(string.Format("<span>{0}</span>", webSession.DisplayName));
           builder.Append(" <i class=\"fa fa-angle-down\"></i>");
           builder.Append("</a>");
           builder.Append("</span>");
            */
            return builder.ToString();
        }

        public static string GenerateUserNotification(ISessionContext context)
        {
            var builder = new StringBuilder();


            builder.Append("<ul class='header-dropdown-list hidden-xs padding-5' id='mobile-profile-img'>");
            builder.Append("<li class=''>");
            builder.Append(
                "<a data-toggle='dropdown' class='dropdown-toggle no-margin userdropdown' href='#' aria-expanded='false'> ");
            builder.Append("<img id='activity-avatar-image' src='/_up-services/user/avatar-image' alt='" + context.WebSession.DisplayName + "' />");
            builder.Append("</a>");
            builder.Append("<ul class='dropdown-menu pull-right'>");
            builder.Append("<li>");
            builder.Append(
                string.Format(
                    "<a class='padding-10 padding-top-0 padding-bottom-0' href='{0}/profile'><i class='fa fa-user'></i> <u>P</u>rofile</a>",
                    context.ApplicationRoute));
            builder.Append("</li>");
            builder.Append("<li class='divider'></li>");
            builder.Append("<li>");
            builder.Append(
                string.Format(
                    "<a data-action='userLogout' class='padding-10 padding-top-5 padding-bottom-5' href='{0}/logout'><i class='fa fa-sign-out fa-lg'></i> <strong><u>L</u>ogout</strong></a>",
                    context.ApplicationRoute));
            builder.Append("</li>");
            builder.Append("</ul>");
            builder.Append("</li>");
            builder.Append("</ul>");

            //builder.Append("<span id='activity' class='activity-dropdown'>");
            //builder.Append("<a href='#' id='user-menu-buttom' title='" + context.WebSession.UserName + "'>");

            //// Update to reflect the output here..

            //builder.Append("<img id='activity-avatar-image' src='/_up-services/user/avatar-image' alt='" + context.WebSession.DisplayName + "' />");

            //builder.Append("</a>");
            //builder.Append("</span>");

            //builder.Append("<div id='user-activity-container' class='ajax-dropdown'>");

            //builder.Append("<div class='navbar-content'>");
            //builder.Append("<div class='row'>");
            //builder.Append("<div class='col-md-12'>");
            //builder.Append("<div class='pull-left'>");

            //builder.Append("<span>" + context.WebSession.DisplayName + "</span><br/>");

            //builder.Append("<span class='text-muted small'>");
            //builder.Append(context.WebSession.EMail ?? "(No Email)");
            //builder.Append("</span>");

            //builder.Append("</div>");

            //builder.Append("<div class='row'>");
            //builder.Append("<div class='col-md-12'>");

            //builder.Append("<div class='pull-right'>");
            //builder.Append(string.Format("<a href='{0}/profile' class='btn btn-primary btn-sm active' style='margin-right:10px;'>View Profile</a>",context.ApplicationRoute));
            //builder.Append(
            //    string.Format(
            //        "<a href='{0}/logout' title='Sign Out' class='btn btn-default btn-sm' data-action='userLogout' data-logout-msg='You can improve your security further after logging out by closing this opened browser'>Sign Out</a>",
            //        context.ApplicationRoute));
            //builder.Append("</div>");
            //builder.Append("</div>");
            //builder.Append("</div>");
            //builder.Append("</div>");

            //builder.Append("</div>");

            return builder.ToString();
        }

        public static string GenerateStyledBreadcrumb(ISessionContext context, IMenu menu,
                                                      string groupClass = "pull-left")
        {
            var builder = new StringBuilder();
            IDensityMenuItem currentMenu = menu.GetCurrentMenuItem();
            List<IDensityMenuItem> menuItems = menu.GetMenu();
            IDensityMenuItem homeMenu = menuItems.FirstOrDefault(x => x.MenuKey == menu.DefaultMenuItemGuid.ToString());

            builder.Append("<div class='breadcrumb-container hidden-xs'>");

            builder.Append(string.Format("<div class='btn-group btn-breadcrumb {0}' >", groupClass));

            if (homeMenu != null)
            {
                builder.Append(StyledBreadcrumbLink(context, currentMenu, homeMenu,
                                                    "<i class='glyphicon glyphicon-home'></i>"));
            }

            if (homeMenu == null || currentMenu.MenuKey != homeMenu.MenuKey)
            {
                builder.Append(StyledBreadcrumbLink(context, currentMenu, currentMenu));
            }

            if (!string.IsNullOrEmpty(context.Page.EntityName))
            {
                builder.Append("<a href='#' class='btn btn-default active'>");
                builder.Append(context.Page.EntityName);
                builder.Append("</a>");
            }

            builder.Append("</div>");

            builder.Append("</div>");

            return builder.ToString();
        }

        public static string GenerateBreadcrumb(ISessionContext context, IMenu menu)
        {
            var builder = new StringBuilder();
            IDensityMenuItem currentMenu = menu.GetCurrentMenuItem();
            List<IDensityMenuItem> menuItems = menu.GetMenu();
            IDensityMenuItem homeMenu = menuItems.FirstOrDefault(x => x.MenuKey == menu.DefaultMenuItemGuid.ToString());

            builder.Append("<ol class='breadcrumb'>");

            if (homeMenu != null)
            {
                builder.Append(BreadcrumbLink(context, currentMenu, homeMenu));
            }

            if (homeMenu == null || currentMenu.MenuKey != homeMenu.MenuKey)
            {
                builder.Append(BreadcrumbLink(context, currentMenu, currentMenu));
            }

            if (!string.IsNullOrEmpty(context.Page.EntityName))
            {
                builder.Append("<li class='active'>");
                builder.Append(context.Page.EntityName);
                builder.Append("</li>");
            }

            builder.Append("</ol>");

            return builder.ToString();
        }

        private static string BreadcrumbLink(ISessionContext context, IDensityMenuItem currentMenu,
                                             IDensityMenuItem menuItem)
        {
            var builder = new StringBuilder();

            if (menuItem.Parent != null)
            {
                builder.Append(BreadcrumbLink(context, currentMenu, menuItem.Parent));
            }

            if (menuItem.Module == null)
            {
                return builder.ToString();
            }

            builder.Append("<li");
            if (currentMenu.MenuKey == menuItem.MenuKey && string.IsNullOrEmpty(context.Page.EntityName))
            {
                builder.Append(" class='active'");
            }
            builder.Append(">");

            if (currentMenu.MenuKey != menuItem.MenuKey || !string.IsNullOrEmpty(context.Page.EntityName))
            {
                builder.Append("<a ");
                if (!string.IsNullOrEmpty(menuItem.Module.FullPath))
                {
                    builder.Append("href='" + menuItem.Module.FullPath + "'");
                }

                builder.Append(" title='" + menuItem.Title + "'>");
            }

            builder.Append(menuItem.Title);

            if (currentMenu.MenuKey != menuItem.MenuKey || !string.IsNullOrEmpty(context.Page.EntityName))
            {
                builder.Append("</a>");
            }

            builder.Append("</li>");

            return builder.ToString();
        }

        private static string StyledBreadcrumbLink(ISessionContext context, IDensityMenuItem currentMenu,
                                                   IDensityMenuItem menuItem, string title = null, string style = null)
        {
            var builder = new StringBuilder();

            if (menuItem.Parent != null)
            {
                builder.Append(StyledBreadcrumbLink(context, currentMenu, menuItem.Parent));
            }

            if (menuItem.Module == null)
            {
                return builder.ToString();
            }

            builder.Append("<a ");

            if (!string.IsNullOrEmpty(style))
            {
                builder.Append(string.Format("style='{0}' ", style));
            }

            if (currentMenu.MenuKey == menuItem.MenuKey && string.IsNullOrEmpty(context.Page.EntityName))
            {
                builder.Append(" class='btn btn-default active' ");
            }

            if (currentMenu.MenuKey != menuItem.MenuKey || !string.IsNullOrEmpty(context.Page.EntityName))
            {
                builder.Append(" class='btn btn-default'  ");

                if (!string.IsNullOrEmpty(menuItem.Module.FullPath))
                {
                    builder.Append(" href='" + menuItem.Module.FullPath + "' ");
                }

                builder.Append(" title='" + menuItem.Title + "'");
            }

            builder.Append(">");

            if (string.IsNullOrEmpty(title))
            {
                title = menuItem.Title;
            }

            builder.Append(title);

            if (currentMenu.MenuKey != menuItem.MenuKey || !string.IsNullOrEmpty(context.Page.EntityName))
            {
                //builder.Append("</a>");
            }

            builder.Append("</a>");

            return builder.ToString();
        }
    }
}
