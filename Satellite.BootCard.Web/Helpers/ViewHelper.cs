﻿using System;
using Universal.Access;
using Universal.Access.Definition;
using Universal.Common.Collections;
using Universal.Common.WebServer.Views;
using System.Web;
namespace Satellite.Demo.Web.Helpers
{
    public class ViewHelper : IViewHelper
    {
        private static ViewHelper _instance;

        public static ViewHelper Instance
        {
            get { return _instance ?? (_instance = new ViewHelper()); }
        }

        [ThreadStatic]
        private static OrderedStringList _documentReady;

        [ThreadStatic]
        private static OrderedStringList _scripts;

        [ThreadStatic]
        private static OrderedStringList _styles;

        [ThreadStatic]
        private static OrderedStringList _stylesheets;

        public static OrderedStringList DocumentReady
        {
            get
            {
                return _documentReady ?? (_documentReady = new OrderedStringList());
            }
        }

        OrderedStringList IViewHelper.Scripts
        {
            get { return Scripts; }
        }

        OrderedStringList IViewHelper.Styles
        {
            get { return Styles; }
        }

        OrderedStringList IViewHelper.Stylesheets
        {
            get { return Stylesheets; }
        }

        OrderedStringList IViewHelper.DocumentReady
        {
            get { return DocumentReady; }
        }

        public static OrderedStringList Scripts
        {
            get
            {
                return _scripts ?? (_scripts = new OrderedStringList());
            }
        }

        public static OrderedStringList Styles
        {
            get
            {
                return _styles ?? (_styles = new OrderedStringList());
            }
        }

        public static OrderedStringList Stylesheets
        {
            get
            {
                return _stylesheets ?? (_stylesheets = new OrderedStringList());
            }
        }

        public static void Clear()
        {
            Stylesheets.Clear();
            Styles.Clear();
            Scripts.Clear();
            DocumentReady.Clear();
        }

        public static string HtmlEncode(GuidString guid)
        {
            return HtmlEncode(guid.ToString());
        }

        public static string UrlEncode(GuidString guid)
        {
            return UrlEncode(guid.ToString());
        }

        public static string UrlEncode(string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                return HttpUtility.UrlEncode(text);
            }
            return string.Empty;
        }

        public static string HtmlEncode(string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                return HttpUtility.HtmlEncode(text);
            }
            return string.Empty;
        }

        public static string SafeEntityEditLink(IAccessInstance entity)
        {
            return SafeEntityEditLink((IEntity)entity);
        }

        public static string SafeEntityEditLink(IEntity entity)
        {
            if (entity == null)
            {
                return string.Empty;
            }

            var guid = entity.PrimaryGuidField.GetStringValue(entity);
            var name = entity.GetType().GetProperty("Name").GetValue(entity);
            return SafeEntityEditLink(guid, name.ToString());
        }

        public static string SafeEntityEditLink(string guid, string title)
        {
            //var builder = new StringBuilder();
            //builder.Append("<span class='callisto-table-span-name' >");
            //builder.Append("<a href=\"Edit/?Guid=" + guid + "\">" + HttpUtility.HtmlEncode(title) + " </a>");
            //builder.Append("</span>");
            //return builder.ToString();
            return "";
        }

        public static string SafeExtensionLink(GuidString extensionGuid)
        {
            //var extension = CoreDataRepository.InternalServiceRepository.Extension.GetByExtensionGuid(extensionGuid);
            //if (extension == null)
            //{
            //    return string.Empty;
            //}

            //var builder = new StringBuilder();
            //builder.Append("<span class='extensionLinkColumn' >");
            //builder.Append("<a href=\"/extensions/Edit/?Guid=" + extension.ExtensionGuid + "\">" + HttpUtility.HtmlEncode(extension.Name) + " <i class=\"fa fa-external-link\"></i></a>");
            //builder.Append("</span>");
            //return builder.ToString();
            return "";
        }
    }
}
