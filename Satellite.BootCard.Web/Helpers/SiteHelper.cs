﻿using System;
using System.Collections.Generic;
using System.Text;
using Common.Logging;
using Universal.Access;
using Universal.Access.WebServer;
using Universal.Common;
using Universal.Common.Density;
using Universal.Common.License;
using Universal.Common.Menu;
using Universal.Core.Density;
using Universal.Core.License;
using Universal.Core.Logging;
using Universal.Core.Platform;

namespace Satellite.Demo.Web.Helpers
{
    public static class SiteHelper
    {
        public static string DocumentationUrl =
            "";

        public static IMenu Menu;
        public static IMenu DemoMenu;

        public static string PageTitle(string title)
        {
            if (string.IsNullOrEmpty(title))
            {
                return title;
            }

            string platformTitle = "Demo";
            if (string.IsNullOrEmpty(platformTitle))
            {
                platformTitle = "Demo [UP: ${UP-PlatformVersion}.${UP-PlatformBuild}]";
            }

            return string.Format("{0} - {1}", platformTitle.ExpandVariables(), title);
        }

        public static void SetPageTitle(ISessionContext context, string title)
        {
            var admin = InternalService.PlatformContext.PromoteToAdministrator();

            try
            {
                context.Page.PageTitle = title;
            }
            finally
            {
                admin.Detach();
            }
        }

        public static ILog GetLogger(Type type)
        {
            return LoggingService.Instance.GetLogger(type);
        }

        public static ILog GetLogger(Type type, GuidString applicationGuid)
        {
            return LoggingService.Instance.GetLogger(type, applicationGuid);
        }

        public static string GenerateMenu(ISessionContext sessionContext)
        {
            var builder = new StringBuilder();

            List<IDensityMenuItem> parentMenuItems = Menu.GetMenu();
            IDensityMenuItem currentMenuItem = Menu.GetCurrentMenuItem();

            if (parentMenuItems != null)
            {
                foreach (IDensityMenuItem parentMenuItem in parentMenuItems)
                {
                    GenerateNavItem(sessionContext, builder, parentMenuItem, currentMenuItem);
                }
            }

            return builder.ToString();
        }

        /// <summary>
        ///     Generates the footer contents.
        /// </summary>
        /// <returns>
        ///     The <see cref="string" />.
        /// </returns>
        /// <remarks>
        /// </remarks>
        public static string GenerateFooterContents()
        {
            var builder = new StringBuilder();

            builder.Append("<div class=\"row\">");
            builder.Append("<div class=\"col-xs-12 col-sm-6 font-sm hidden-xs\">");
            builder.Append("<span class=\"txt-color-white\">" + PlatformContext.Instance.PlatformCopyright);
            builder.Append("<small class=\"hidden-sm hidden-md\"><br/>" + PlatformContext.Instance.PlatformPatents +
                           "</small><br/>");
            builder.Append("</span>");
            builder.Append("</div>");

            builder.Append("<div class=\"col-xs-6 col-sm-6 font-sm text-right hidden-xs\">");
            builder.Append("<div class=\"txt-color-white inline-block\">");
            builder.Append("Licensed to " + LicenseService.Instance.CompanyName);
            builder.Append(", [Instance: " + LicenseService.Instance.InstanceType + ", Plan: " +
                           LicenseService.Instance.PlanType + "] <br/>");
            builder.Append("<small><b>" + PlatformContext.Instance.PlatformDescription + "</b></small>");

            builder.Append("</div>");
            builder.Append("</div>");

            builder.Append("</div>");

            if (LicenseService.CheckForLicenseWarning() || LicenseService.CheckForLicenseError())
            {
                builder.Append("<div id=\"notification-bar\">");
                // show for license expiry or type otherwise display:none
            }
            else
            {
                builder.Append("<div id=\"notification-bar\" style=\"display:none;\">");
            }

            builder.Append("<br/>");
            builder.Append("<br/>");
            builder.Append("<br/>");
            builder.Append("<div class=\"navbar navbar-fixed-bottom navbar-inverse\">");

            if (LicenseService.CheckForLicenseError())
            {
                builder.Append("<div class=\"errorbar-inner\">");

                if (LicenseService.Instance.UserHardLimit)
                {
                    builder.Append(
                        string.Format(
                            "You have exceeded the number of active users permitted by your license. You currently have {0} active users out of {1} allowed. Please contact your supplier or you will be unable to continue using some features of the Universal Platform.",
                            LicenseService.Instance.CurrentActiveUsers,
                            LicenseService.Instance.UserLimit));
                    builder.Append("<br />");
                }

                if (LicenseService.Instance.OrganisationHardLimit)
                {
                    builder.Append(
                        string.Format(
                            "You have exceeded the number of organsiations permitted by your license. You currently have {0} organisations out of {1} allowed. Please contact your supplier or you will be unable to continue using some features of the Universal Platform.",
                            LicenseService.Instance.CurrentOrganisations,
                            LicenseService.Instance.OrganisationLimit));
                    builder.Append("<br />");
                }

                builder.Append("</div>");
            }

            if (!LicenseService.CheckForLicenseWarning())
            {
                builder.Append("<div id=\"warning-bar\" class=\"warningbar-inner\" style=\"display:none\">");
            }
            else
            {
                builder.Append("<div id=\"warning-bar\" class=\"warningbar-inner\">");
            }

            builder.Append("<div class=\"iebar-inner\" style=\"display:none\">");
            builder.Append(
                "Microsoft Internet Explorer is not fully supported. Some functions will not operate as expected.");
            builder.Append("<br />");
            builder.Append("</div>");

            if (LicenseService.Instance.UserSoftLimit)
            {
                builder.Append(
                    string.Format(
                        "You have exceeded the number of active users permitted by your license. You currently have {0} active users out of {1} allowed. Please contact your supplier or you will be unable to continue using some features of the Universal Platform.",
                        LicenseService.Instance.CurrentActiveUsers, LicenseService.Instance.UserLimit));
                builder.Append("<br />");
            }

            if (LicenseService.Instance.LicenseType != LicenseType.Production)
            {
                builder.Append(
                    string.Format(
                        "The system is operating under a {0} license, and cannot be used for production purposes.",
                        LicenseService.Instance.LicenseType));
                builder.Append("<br />");
            }

            if (LicenseService.Instance.ExpiryDate != null)
            {
                double daysUntilExpiry = (LicenseService.Instance.ExpiryDate.Value.Date - DateTime.Now.Date).TotalDays;
                if (LicenseService.Instance.ExpiryDate.Value.Date != new DateTime().Date)
                {
                    if (daysUntilExpiry <= 30 && daysUntilExpiry > 0)
                    {
                        builder.Append(string.Format("Your Universal Platform License will expire in {0} day{1}.",
                            daysUntilExpiry, daysUntilExpiry == 1 ? string.Empty : "s"));
                    }
                    else if (daysUntilExpiry <= 0 && daysUntilExpiry > -7)
                    {
                        builder.Append(
                            string.Format(
                                "Your Universal Platform License has expired. Please contact your supplier within {0} day{1} or you will be unable to continue using the Universal Platform.",
                                daysUntilExpiry + 7,
                                daysUntilExpiry == -6 ? string.Empty : "s"));
                    }
                    else if (daysUntilExpiry <= -7)
                    {
                        builder.Append(
                            "Your Universal Platform License has expired. Please contact your supplier or you will be unable to continue using the Universal Platform.");
                    }
                }
            }

            builder.Append("</div>"); // warning-bar
            builder.Append("</div>"); // navbar
            builder.Append("</div>"); // notification-bar
            builder.Append("</div>"); // footer

            ViewHelper.DocumentReady.Add(
                "if($.browser.msie){ $('#notification-bar').show();  $('#warning-bar').show(); $('.iebar-inner').show(); }");

            return builder.ToString();
        }

        public static string GenerateMainMenu()
        {
            var builder = new StringBuilder();

            List<IDensityMenuItem> menuItems = Menu.GetMenu();
            IDensityMenuItem rootMenuItem = Menu.GetRootMenuItem();

            builder.Append("<ul class='nav navbar-nav'>");

            foreach (var densityMenuItem1 in menuItems)
            {
                var densityMenuItem = (DensityMenuItem) densityMenuItem1;
                string path = "#";
                if (densityMenuItem.Module != null)
                {
                    path = densityMenuItem.Module.FullPath;
                }

                if (densityMenuItem.Equals(rootMenuItem))
                {
                    builder.Append("<li class='active' ><a title='" + densityMenuItem.ToolTip + "' href='" + path +
                                   "' >" + densityMenuItem.Title + "</a></li>");
                }
                else
                {
                    builder.Append("<li><a title='" + densityMenuItem.ToolTip + "' href='" + path + "' >" +
                                   densityMenuItem.Title + "</a></li>");
                }
            }

            builder.Append("</ul>");

            return builder.ToString();
        }

        /// <summary>
        ///     Generates the nav item.
        /// </summary>
        /// <param name="sessionContext"></param>
        /// <param name="builder">
        ///     The builder.
        /// </param>
        /// <param name="menuItem">
        ///     The menu item.
        /// </param>
        /// <param name="currentMenuItem">
        ///     The current menu item.
        /// </param>
        /// <remarks>
        /// </remarks>
        public static void GenerateNavItem(ISessionContext sessionContext, StringBuilder builder,
            IDensityMenuItem menuItem, IDensityMenuItem currentMenuItem)
        {
            if (!menuItem.ShowInList) return;

            string modulePath = string.Empty;
            if (menuItem.Module != null)
            {
                modulePath = menuItem.Module.FullPath;
            }

            if (!string.IsNullOrEmpty(modulePath))
            {
                if (menuItem.MenuKey.Equals(currentMenuItem.MenuKey))
                {
                    Menu.SetCurrentMenu(menuItem);
                    menuItem.IsActive = true;
                }
                else
                {
                    menuItem.IsActive = false;
                }
            }

            string enabledClass = string.Empty;
            IDensityModule module = menuItem.Module;
            if (module != null)
            {
                if (!string.IsNullOrEmpty(module.ModuleClass))
                {
                    //if (!GetLicenseHelperSession(sessionContext).IsFeatureAvailableByModuleClass(module.ModuleClass))
                    //{
                    //    enabledClass = "disabled";
                    //    modulePath = "#";
                    //}
                }
            }

            builder.Append("<li");
            if (menuItem.IsActive)
            {
                builder.Append(" class=\"active\"");
            }

            builder.Append(">");

            builder.Append("<a ");
            if (!string.IsNullOrEmpty(modulePath))
            {
                builder.Append("href=\"" + modulePath + "\"");
            }

            builder.Append("title=\"" + menuItem.Title + "\"><i class=\"fa-lg fa-fw ");

            if (!string.IsNullOrEmpty(menuItem.IconResourceValue))
            {
                builder.Append(menuItem.IconResourceValue);
            }
            else
            {
                builder.Append("fa fa-cog");
            }

            builder.Append(" \"></i> <span class=\"menu-item-parent " + enabledClass + "\">" + menuItem.Title +
                           "</span></a>");

            if (menuItem.MenuItems.Count > 0)
            {
                builder.Append("<ul>");
            }

            foreach (IDensityMenuItem subMenuItem in menuItem.MenuItems)
            {
                GenerateNavItem(sessionContext, builder, subMenuItem, currentMenuItem);
            }

            if (menuItem.MenuItems.Count > 0)
            {
                builder.Append("</ul>");
            }

            builder.Append("</li>");
        }

        public static string GenerateUserMenu(IWebSession webSession)
        {
            var builder = new StringBuilder();

            if (webSession.Principal != null)
            {
                builder.Append("<li class=\"dropdown\">");
                builder.Append(
                    "<a data-toggle=\"dropdown\" class=\"dropdown-toggle\" href=\"#\" id=\"dropdownMenu\"> " +
                    webSession.DisplayName + " <b class=\"caret\"></b></a>");
                builder.Append("<ul class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"dropdownMenu\" >");
                builder.Append(
                    "<li><a id=\"user-profile\" name=\"user-profile\" href=\"#\" tabindex=\"-1\" ><i class=\"fa fa-user\"></i> Account</a></li>");
                builder.Append(
                    "<li><a id=\"user-permissions\" name=\"user-permissions\" href=\"#\" tabindex=\"-1\" ><i class=\"fa fa-lock\"></i> Permissions</a></li>");
                builder.Append("<li class=\"divider\"></li>");
                builder.Append(
                    "<li><a tabindex=\"-1\" href=\"/account/logout/\"><i class=\"fa fa-sign-out\"></i> Logout</a></li>");
                builder.Append("</ul>");
                builder.Append("</li>");
            }

            return builder.ToString();
        }

        public static string GenerateBrandLink(ISessionContext context)
        {
            var builder = new StringBuilder();
            builder.Append(
                "<a title=\"Universal Platform\" class=\"navbar-brand\" href=\"/\"><img src=\"" +
                context.ApplicationRoute +
                "/core/images/omnieffect/oe_universal_logo.png\" alt=\"Universal Platorm\" /></a>");
            return (builder.ToString());
        }
    }
}
