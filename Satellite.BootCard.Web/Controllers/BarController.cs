﻿using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Newtonsoft.Json;
using Satellite.Demo.Web.Controllers.Shared;
using Satellite.Demo.Web.Helpers;
using Satellite.Demo.Web.Models;
using Universal.Access.Application;
using Universal.Access.Mvc;
using Universal.Access.WebServer;
using Universal.Common.Applications;
using Universal.Common.Extensions;
using Universal.Common.WebServer;
using Universal.Common.WebServer.ActionResults;
using Universal.Common.WebServer.Attributes;

namespace Satellite.Demo.Web.Controllers
{
    [ApplicationModule("79DF8C78-F887-4E18-8A22-BBA261856B01", DotNetMvcApplicationModuleType.ExtensionGuidString, "Bar Controller", "Bar", "/bar/")]
    public class BarController : BaseDemoController<BarController>
    {
        private const string PageTitle = "Bar";

        public IViewData Index()
        {
            IPage page = SessionContext.Page;
            page.PageTitle = SiteHelper.PageTitle(PageTitle);
            ViewData["page"] = page;
            ViewData["title"] = PageTitle;
            ViewData["websession"] = SessionContext.WebSession;
            ViewData["context"] = SessionContext;
            return Render();
        }

        [Public]
        [ValidFor(Method.Get)]
        public IActionResult GetChartData()
        {
            var content = new StringContent
            {
                ContentType = "application/json;charset=UTF-8"
            };

            string fileData;

            var location = Assembly.GetExecutingAssembly().Location;
            var ar = location.Split('\\');
            var amount = ar.Length - 2;
            var sb = new StringBuilder();
            for (int i = 0; i < amount; i++)
            {
                sb.Append(ar[i]);
                sb.Append('\\');
            }

            using (var outfile = new StreamReader(sb + @"\www\JsonData\Data.json"))
            {
                fileData = outfile.ReadToEnd();
            }

            var x = JsonConvert.DeserializeObject<Result>(fileData);
            var stuffs = x.Names.Select((t, i) => new Bar
            {
                name = t, data = new []{x.Values[i]}
            }).ToList();

            var result = new
            {
                Series = stuffs.ToArray()
            };

            content.Body = JsonConvert.SerializeObject(result);
            return content;
        }
    }
}

