﻿using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Newtonsoft.Json;
using Satellite.Demo.Web.Controllers.Shared;
using Satellite.Demo.Web.Helpers;
using Satellite.Demo.Web.Models;
using Universal.Access.Application;
using Universal.Access.Mvc;
using Universal.Access.WebServer;
using Universal.Common.Applications;
using Universal.Common.Extensions;
using Universal.Common.Parameters;
using Universal.Common.WebServer;
using Universal.Common.WebServer.ActionResults;
using Universal.Common.WebServer.Attributes;

namespace Satellite.Demo.Web.Controllers
{
    [ApplicationModule("6EB8888D-F391-41D7-BC9B-B1921410BE94", DotNetMvcApplicationModuleType.ExtensionGuidString,
        "Demo Controller", "Demo", "/demo/")]
    public class DemoController : BaseDemoController<DemoController>
    {
        private const string PageTitle = "Demo";

        public IViewData Index()
        {
            IPage page = SessionContext.Page;
            page.PageTitle = SiteHelper.PageTitle(PageTitle);
            ViewData["page"] = page;
            ViewData["title"] = PageTitle;
            ViewData["websession"] = SessionContext.WebSession;
            ViewData["context"] = SessionContext;
            return Render();
        }

        [Public]
        [ValidFor(Method.Get)]
        public IActionResult GetChartData()
        {
            var content = new StringContent
            {
                ContentType = "application/json;charset=UTF-8"
            };

            string fileData;

            var location = Assembly.GetExecutingAssembly().Location;
            var ar = location.Split('\\');
            var amount = ar.Length - 2;
            var sb = new StringBuilder();
            for (int i = 0; i < amount; i++)
            {
                sb.Append(ar[i]);
                sb.Append('\\');
            }

            using (var outfile = new StreamReader(sb + @"\www\JsonData\Data.json"))
            {
                fileData = outfile.ReadToEnd();
            }

            var x = JsonConvert.DeserializeObject<Result>(fileData);

            content.Body = JsonConvert.SerializeObject(x);
            return content;
        }

        [Public]
        [ValidFor(Method.Get)]
        public IActionResult GetContacts()
        {
            var content = new StringContent
            {
                ContentType = "application/json;charset=UTF-8"
            };

            string data = GetFileData();

            var contacts = JsonConvert.DeserializeObject<Contact[]>(data);

            content.Body = JsonConvert.SerializeObject(contacts);
            return content;
        }

        [Public]
        [ValidFor(Method.Get)]
        public IActionResult GetDetails()
        {
            var content = new StringContent
            {
                ContentType = "application/json;charset=UTF-8"
            };
            var id = Parameters.GetParameterAsInt("id");
            string data = GetFileData();

            var contact = JsonConvert.DeserializeObject<Contact[]>(data).FirstOrDefault(x => x.ID == id);
            contact.SerializedObj = JsonConvert.SerializeObject(contact);
            content.Body = JsonConvert.SerializeObject(contact);
            return content;
        }

        [Public]
        [ValidFor(Method.Get)]
        public IActionResult Save()
        {
            var content = new StringContent
            {
                ContentType = "application/json;charset=UTF-8"
            };
            var isNew = Parameters.GetParameterAsBool("isNew");
            var id = Parameters.GetParameterAsInt("ContactId");
            var name = Parameters["name"];
            var company = Parameters["company"];
            var dept = Parameters["dept"];
            var phone = Parameters["phone"];
            var email = Parameters["email"];
            var jobTitle = Parameters["jobTitle"];

            string data = GetFileData();

            var contacts = JsonConvert.DeserializeObject<Contact[]>(data);

            var contactList = contacts.ToList();

            if (isNew)
            {
                contactList.Add(new Contact
                    {
                        Name = name,
                        Company = company,
                        ID = contactList.Count + 1,
                        Details = new Details
                        {
                            Dept = dept,
                            Email = email,
                            JobTitle = jobTitle,
                            Phone = phone
                        }
                    }); 
            }
            else
            {
                var contact = contactList.FirstOrDefault(x => x.ID == id);
                contact.Name = name;
                contact.Company = company;
                contact.Details.Phone = phone;
                contact.Details.Dept = dept;
                contact.Details.Email = email;
                contact.Details.JobTitle = jobTitle;
            }

            if (WriteFileData(JsonConvert.SerializeObject(contactList.ToArray())))
            {
                content.Body = JsonConvert.SerializeObject(contactList.ToArray()); 
            }
            return content;
            
        }
        [Public]
        [ValidFor(Method.Get)]
        public IActionResult Remove()
        {
            var content = new StringContent
            {
                ContentType = "application/json;charset=UTF-8"
            };
            
            var id = Parameters.GetParameterAsInt("ContactId");

            string data = GetFileData();

            var contacts = JsonConvert.DeserializeObject<Contact[]>(data);

            var contactList = contacts.ToList();

            contactList.Remove(contactList.FirstOrDefault(x => x.ID == id));

            if (WriteFileData(JsonConvert.SerializeObject(contactList.ToArray())))
            {
                content.Body = JsonConvert.SerializeObject(contactList.ToArray());
            }
            return content;
        }

        [Public]
        [ValidFor(Method.Get)]
        public IActionResult Reset()
        {
            var content = new StringContent
            {
                ContentType = "application/json;charset=UTF-8"
            };
            string fileData;

            var location = Assembly.GetExecutingAssembly().Location;
            var ar = location.Split('\\');
            var amount = ar.Length - 2;
            var sb = new StringBuilder();
            for (int i = 0; i < amount; i++)
            {
                sb.Append(ar[i]);
                sb.Append('\\');
            }

            using (var outfile = new StreamReader(sb + @"\www\JsonData\OriginalReset.json"))
            {
                fileData = outfile.ReadToEnd();
            }
            WriteFileData(fileData);

            return content;
        }

        private static string GetFileData()
        {
            string fileData;

            var location = Assembly.GetExecutingAssembly().Location;
            var ar = location.Split('\\');
            var amount = ar.Length - 2;
            var sb = new StringBuilder();
            for (int i = 0; i < amount; i++)
            {
                sb.Append(ar[i]);
                sb.Append('\\');
            }

            using (var outfile = new StreamReader(sb + @"\www\JsonData\Contacts.json"))
            {
                fileData = outfile.ReadToEnd();
            }
            return fileData;
        }
        private static bool WriteFileData(string data)
        {
            try
            {
                var location = Assembly.GetExecutingAssembly().Location;
                var ar = location.Split('\\');
                var amount = ar.Length - 2;
                var sb = new StringBuilder();
                for (int i = 0; i < amount; i++)
                {
                    sb.Append(ar[i]);
                    sb.Append('\\');
                }

                using (var infile = new StreamWriter(sb + @"\www\JsonData\Contacts.json"))
                {
                    infile.Write(data);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
