﻿using System;
using Satellite.Demo.Web.Helpers;
using Universal.Access;
using Universal.Common.WebServer;
using Universal.Common.WebServer.Logging;

namespace Satellite.Demo.Web.Controllers.Shared
{
    public class BaseDemoController<T> : BaseSecureApplicationModuleController<T> 
        where T : BaseSecureApplicationModuleController<T>
    {
        public BaseDemoController() : base(SiteHelper.DemoMenu)
        {
        }
        public new static ILogger GetLogger(Type type)
        {
            return GetLogger(type, new GuidString(DemoWebApplication.ApplicationGuidString));
        }
    }
}
