﻿using System.Threading.Tasks;
using Universal.Access.Entities;
using Universal.Common.Account;
using Universal.Common.Applications;
using Universal.Common.Extensions;
using Universal.Common.Utils;

namespace Satellite.Demo.Web.Controllers
{
    [AccountController]
    [ApplicationModule("3c5295ea-4e22-43d2-8082-106b035f2f1d", DotNetMvcApplicationModuleType.ExtensionGuidString,
        "Account Controller", "Account", "/account/")]
    public class AccountController : BaseAccountController
    {
        private static async Task SendEmailAsync(IUser user, string content, string subject)
        {
            SendEmail(user, content, subject);
        }

        private static void SendEmail(IUser user, string content, string subject)
        {
            EmailUtils.SendEmail(user.Email, content, subject);
        }
    }
}
