﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Satellite.Demo.Web.Models
{
    public class Details
    {
        public string JobTitle { get; set; }
        public string Dept { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }
}
