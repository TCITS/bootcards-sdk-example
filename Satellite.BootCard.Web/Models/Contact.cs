﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Satellite.Demo.Web.Models
{
    public class Contact
    {
        public string SerializedObj { get; set; }
        public int ID { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string Company { get; set; }
        public Details Details { get; set; }
    }
}
