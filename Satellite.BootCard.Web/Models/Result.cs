﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Satellite.Demo.Web.Models
{
    public class Result
    {
        public string[] Names { get; set; }
        public int[] Values { get; set; }
    }
}
