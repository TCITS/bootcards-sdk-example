﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Satellite.Demo.Web.Models
{
    public class Bar
    {
        public string name { get; set; }
        public int[] data { get; set; }
    }
}
