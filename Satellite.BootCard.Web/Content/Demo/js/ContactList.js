﻿var demoBaseURI = "${UP-AppRoute}/demo/";

function CreateContactData(jsonData) {
    var elChart = $("#groupList");
    var stuffs = "";
    for (var i = 0; i < jsonData.length; i++) {
        stuffs = stuffs + "<a class='list-group-item pjax' onclick='GetDetails(" + jsonData[i].ID + ")'><img src='${UP-AppRoute}/bootcards/img/" + jsonData[i].Image + "' class='img-rounded pull-left'>" +
            "<h4 class='list-group-item-heading'>" + jsonData[i].Name + "</h4><p class='list-group-item-text'>" + jsonData[i].Company + "</p></a>";
    }
    elChart.html(stuffs);
}
function LoadData(endpoint, successHandler) {
    $.ajax({
        url: demoBaseURI + endpoint,
        type: "GET",
        dataType: "json",
        success: successHandler,
        error: function () { }
    });
}
function LoadDetails(endpoint, id, successHandler) {
    $.ajax({
        url: demoBaseURI + endpoint + "?id=" + id,
        type: "GET",
        dataType: "json",
        success: successHandler,
        error: function () { }
    });
}
$(document).ready(function () {
    LoadData("GetContacts", function (data) {
        CreateContactData(data);
        GetDetails(1);
    });
});

function GetDetails(id) {
    LoadDetails("GetDetails", id, function(data) {
        PopulateDetails(data);
    });
}
function ClearDetails() {
    var el = $("#contactCard");
    var detailsHtml = "<div class='panel panel-default'><div class='panel-heading clearfix'><h3 class='panel-title pull-left'>Contact Details</h3>" +
        "<div class='btn-group pull-right visible-xs'><a class='btn btn-primary' data-toggle='modal' data-target='#editModal'>" +
        "<i class='fa fa-pencil'></i><span>Edit</span></a></div><a class='btn btn-primary pull-right hidden-xs' data-contact='' data-toggle='modal' data-target='#editModal'>" +
        "<i class='fa fa-pencil'></i><span>Edit</span></a></div><div class='list-group'><div class='list-group-item'>" +
        "<img src='${UP-AppRoute}/bootcards/img/' class='img-rounded pull-left'><label>Name</label><h4 class='list-group-item-heading'></h4></div><div class='list-group-item'>" +
        "<label>Company</label><h4 class='list-group-item-heading'></h4></div><div class='list-group-item'><label>Job Title</label>" +
        "<h4 class='list-group-item-heading'></h4></div><div class='list-group-item'><label>Department</label><h4 class='list-group-item-heading'></h4>" +
        "</div><a class='list-group-item' href='tel://'><label>Phone</label><h4 class='list-group-item-heading'></h4></a>" +
        "<a class='list-group-item' href='mailto:'><label>Email</label><h4 class='list-group-item-heading'></h4></a>" +
        "</div><div class='panel-footer'><small class='pull-left'>Built with Bootcards - Base Card</small></div></div>";
    el.html(detailsHtml);
}

function PopulateDetails(data) {
    var el = $("#contactCard");
    var detailsHtml = "<div class='panel panel-default'><div class='panel-heading clearfix'><h3 class='panel-title pull-left'>Contact Details</h3>" +
        "<div class='btn-group pull-right visible-xs'><a class='btn btn-primary' data-toggle='modal' data-target='#editModal'>" +
        "<i class='fa fa-pencil'></i><span>Edit</span></a></div><a class='btn btn-primary pull-right hidden-xs' data-contact='" + data.SerializedObj + "' data-toggle='modal' data-target='#editModal'>" +
        "<i class='fa fa-pencil'></i><span>Edit</span></a></div><div class='list-group'><div class='list-group-item'>" +
        "<img src='${UP-AppRoute}/bootcards/img/" + data.Image + "' class='img-rounded pull-left'><label>Name</label><h4 class='list-group-item-heading'>" + data.Name + "</h4></div><div class='list-group-item'>" +
        "<label>Company</label><h4 class='list-group-item-heading'>" + data.Company + "</h4></div><div class='list-group-item'><label>Job Title</label>" +
        "<h4 class='list-group-item-heading'>" + data.Details.JobTitle + "</h4></div><div class='list-group-item'><label>Department</label><h4 class='list-group-item-heading'>" + data.Details.Dept + "</h4>" +
        "</div><a class='list-group-item' href='tel://" + data.Details.Phone + "'><label>Phone</label><h4 class='list-group-item-heading'>" + data.Details.Phone + "</h4></a>" +
        "<a class='list-group-item' href='mailto:" + data.Details.Email + "'><label>Email</label><h4 class='list-group-item-heading'>" + data.Details.Email + "</h4></a>" +
        "</div><div class='panel-footer'><small class='pull-left'>Built with Bootcards - Base Card</small></div></div>";
    el.html(detailsHtml);
}

$('#editModal').on('show.bs.modal', function (e) {

    //get data-id attribute of the clicked element
    var contact = $(e.relatedTarget).data('contact');

    //populate the textbox
    $(e.currentTarget).find("input[name=ContactId]").val(contact.ID);
    $(e.currentTarget).find("input[name=name]").val(contact.Name);
    $(e.currentTarget).find("input[name=company]").val(contact.Company);
    $(e.currentTarget).find("input[name=jobTitle]").val(contact.Details.JobTitle);
    $(e.currentTarget).find("input[name=email]").val(contact.Details.Email);
    $(e.currentTarget).find("input[name=phone]").val(contact.Details.Phone);
    $(e.currentTarget).find("input[name=dept]").val(contact.Details.Dept);
});

$('#addModal').on('show.bs.modal', function (e) {

    //get data-id attribute of the clicked element
    var contact = $(e.relatedTarget).data('contact');

    //populate the textbox
    $(e.currentTarget).find("input[name=ContactId]").val("");
    $(e.currentTarget).find("input[name=name]").val("");
    $(e.currentTarget).find("input[name=company]").val("");
    $(e.currentTarget).find("input[name=jobTitle]").val("");
    $(e.currentTarget).find("input[name=email]").val("");
    $(e.currentTarget).find("input[name=phone]").val("");
    $(e.currentTarget).find("input[name=dept]").val("");
});

$("#addModal").on("click", "#submit-button-add", function (e) {
    e.preventDefault();
    SaveData("addModal", true);
    return false;
});
$("#editModal").on("click", "#submit-button-edit", function (e) {
    e.preventDefault();
    SaveData("editModal", false);
    return false;
});

function SaveData(theForm, isNew) {
    var id = $("#" + theForm).find("input[name=ContactId]").val();
    var name = $("#" + theForm).find("input[name=name]").val();
    var company = $("#" + theForm).find("input[name=company]").val();
    var jobTitle = $("#" + theForm).find("input[name=jobTitle]").val();
    var email = $("#" + theForm).find("input[name=email]").val();
    var phone = $("#" + theForm).find("input[name=phone]").val();
    var dept = $("#" + theForm).find("input[name=dept]").val();

    var x = "?isNew=" + isNew + "&ContactId=" + id + "&name=" + name + "&company=" + company + "&jobTitle=" + jobTitle + "&email=" + email + "&phone=" + phone + "&dept=" + dept;
    var jumpTo = id;
    if (id == null) {
        jumpTo = 1;
    }
    $.ajax({
        url: demoBaseURI + "save" + x, //this is the submit URL
        type: "GET", //or POST
        data: x,
        success: function () {
            LoadData("GetContacts", function (data) {
                CreateContactData(data);
                GetDetails(jumpTo);
            });
        },
        error: function () {
            alert("Error");
        }
    });
}
$("#editModal").on("click", "#submit-button-delete", function (e) {
    e.preventDefault();
    var id = $("#editModal").find("input[name=ContactId]").val();
    $.ajax({
        url: demoBaseURI + "remove" + "?ContactId=" + id, //this is the submit URL
        type: "GET", //or POST
        data: id,
        success: function () {
            LoadData("GetContacts", function (data) {
                CreateContactData(data);
                ClearDetails();
            });
        },
        error: function () {
            alert("Error");
        }
    });
    return false;
});
$(document).on("click", "#submit-button-reset", function (e) {
    $.ajax({
        url: demoBaseURI + "reset", //this is the submit URL
        type: "GET", //or POST
        data: null,
        success: function () {
            LoadData("GetContacts", function (data) {
                CreateContactData(data);
                GetDetails(1);
            });
        },
        error: function () {
            LoadData("GetContacts", function (data) {
                CreateContactData(data);
                GetDetails(1);
            });
        }
    });
return false;
});