﻿var demoBaseURI = "${UP-AppRoute}/";
var demoLoading = "<div style='width: 90px; margin: 0px auto; padding-top: 100px;padding-bottom: 50px;' ><p><img src='${UP-AppRoute}/smartadmin/img/loading.gif' /> Loading ... </p></div>";

function LoadData(el, routing, endpoint, successHandler) {
    el.html(demoLoading);
    $.ajax({
        url: demoBaseURI + routing + "/" + endpoint,
        type: "GET",
        dataType: "json",
        success: successHandler,
        error: function () { }
    });
}

function CreateBarChart(el, jsonData) {

    el.highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Total Coolness'
        },
        subtitle: {
            text: 'Universal Platform'
        },
        xAxis: {
            categories: [
                'Numbers'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Coolness Level'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: jsonData.Series
    });
};

function RefreshChart() {
    RefreshBarGraph();
}

function RefreshBarGraph() {
    var elChart = $('#BarGraph');
    LoadData(elChart, "bar", "GetChartData", function (data) {
        CreateBarChart(elChart, data);
    });
}

$(document).ready(function () {
    $("#btn-refresh-Chart").on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        RefreshChart();
        return false;
    });
    RefreshAllCharts();
});

function RefreshAllCharts() {
    RefreshChart();
}